const Sequelize = require('Sequelize');

const sequelize = new Sequelize('nice', 'root', '1234', {
  host: 'localhost',
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  define: {
    timestamps: false
  },
});

// load models                 
const models = [
  'sceneMode',
  'sceneMark'
];
models.forEach((model) => {
  module.exports[model] = sequelize.import(__dirname + '/' + model);
});

module.exports.sequelize = sequelize;
