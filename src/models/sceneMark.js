/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sceneMark', {
    scenemark_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    scenemark_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    scenemark_status: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    scenemark_sequence: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    scenemark_create_time: {
      type: DataTypes.DATE,
      allowNull: true
    },
    scenemark_update_time: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'sceneMark'
  });
};
