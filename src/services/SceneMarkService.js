const models = require('../models');

function getTrigger() {
  return new Promise(function (resolve) {
    models.sceneMark.findAll({ raw: true }).then((dbResult) => {
      let result = [];
      dbResult.forEach((value) => {
        result.push(value);
      });
      console.log(dbResult);
      resolve(result);
    });
  })
}

module.exports = {
  getTrigger,
};
