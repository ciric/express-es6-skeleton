const models = require('../models');

function sceneMode() {
  return new Promise(function (resolve) {
    models.sceneMode.findAll({ raw: true }).then((dbResult) => {
      let result = [];
      dbResult.forEach((value) => {
        result.push(value);
      });
      resolve(result);
    });
  })
}

module.exports = {
  sceneMode,
};
