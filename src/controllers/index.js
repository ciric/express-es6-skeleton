const express = require('express');

const router = express.Router();
const SceneModeController = require('./SceneModeController');
const SceneMarkController = require('./SceneMarkController');

console.log(123);
router.use('/SceneModes', SceneModeController);
router.use('/SceneMarks', SceneMarkController);
module.exports = router;
