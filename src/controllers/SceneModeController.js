const express = require('express');

const router = express.Router();
const SceneModeService = require('../services/SceneModeService');

router.get('/', (req, res) => {
  console.log('b');
  SceneModeService.sceneMode()
    .then((result) => {
      res.json(result);
    });
});


module.exports = router;
