const express = require('express');
const expressSession = require('express-session');
const controllers = require('./controllers');

const app = express();
const port = process.env.PORT || 3000;


// catch 404 and forward to error handler
// app.use((req, res, next) => {
//   const err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// const multer = require('multer');

app.use(controllers);
app.listen(port, () => {
  console.log(`Listening on port: ${port}`);
  console.log(__dirname);
});
