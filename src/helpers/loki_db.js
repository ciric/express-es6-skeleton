const Loki = require('lokijs');

// module.exports = new Loki('loki.json');

const db = new Loki('loki.db', {
  autoload: true,
  autosave: true,
  autosaveInterval: 4000
});
module.exports = db;
// // implement the autoloadback referenced in loki constructor
