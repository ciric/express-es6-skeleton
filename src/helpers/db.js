const dbConfig = require('config').get('dev.dbConfig');
const Sequelize = require('sequelize');

console.log(dbConfig);
// Sequelize(database, user, password, {})
const sequelize = new Sequelize(dbConfig.dbName, dbConfig.user, dbConfig.password, {
  host: dbConfig.host,
  dialect: dbConfig.dialect, // or 'sqlite', 'postgres', 'mariadb'
  port: dbConfig.port // or 5432 (for postgres)
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  });

module.exports = sequelize;
